<?php include 'header.php' ?>
    
  <body id="iact" class="transition animsition" data-animsition-in-class="fade-in-right-sm"  data-animsition-in-duration="600">
      
      <?php include 'modal.php' ?>
      
      <?php include 'header-menu.php' ?>
      
    <div id="fullpage">
        <div class="section">

            <div id="slide1" class="slide" data-anchor="slide1">
                <div class="caja">
                    &nbsp;
                    
                </div>
            </div>

            <div id="slide2" class="slide" data-anchor="slide2">
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <h1>InnovationACT</h1>
                    </div>
                    <div class="clearfix"></div>
                <div class="col-lg-3 col-lg-offset-0 col-md-3 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 info">
                  <p>Client</p>
                  <div class="clearfix"></div>
                  <p>ANU Technology Transfer Office</p>
                </div>
                <div class="col-lg-3 col-lg-offset-0 col-md-3 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 info">
                  <p>Year</p>
                  <div class="clearfix"></div>
                  <p>2014-Present</p>
                </div>
                <div class="col-lg-3 col-lg-offset-0 col-md-3 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 info">
                  <p>Type</p>
                  <div class="clearfix"></div>
                  <p>Program Design, Workshop Facilitation, Branding, Digital Platform Development.</p>
                </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-6 col-lg-offset-3 col-xs-10 col-xs-offset-1 content">
                        <p>The Brief</p>
                        <p>InnovationACT is Canberra’s largest entrepreneurship program. It was founded in 2008 by a group of research students aiming to improve the commercialisation skills and drive of ANU researchers. In 2013, ANU TTO realised that the program was lacking a consistent structure in line with entrepreneurship education initiatives. We were asked to design and deliver the educational content, brand and organisational model for this important component of Canberra’s innovation ecosystem.
</p>
                        
                    </div>
                </div>
            </div>

            <div id="slide3" class="slide" data-anchor="slide3">
                <div class="caja">
                    &nbsp;
                </div>
            </div>


      </div>
    </div>
      
      <div id="footer-slide" class="container-fluid no-opacity">
          <div class="col-lg-12">
              
              <p><a href="index" class="animsition-link fleft" data-animsition-out-class="fade-out-right-sm" data-animsition-out-duration="300">back to all projects</a></p>
          </div>
      </div>
      
      
      
<? include 'footer.php' ?>