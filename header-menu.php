<header id="header" class="container-fluid">
          <div class="col-lg-3 text-left">
              <a href="index" id="logo-header" class="animsition-link fleft no-opacity" data-animsition-out-class="fade-out-right-sm" data-animsition-out-duration="300"> <img src="img/logo-ninpo.svg" alt=""></a>
          </div>
          <div class="col-lg-1 col-lg-offset-8 text-right">
              <button id="button-modal" type="button" data-toggle="modal" data-target="#flip" class="no-opacity"><img src="img/icon-hamburger.svg" alt=""></button>
          </div>
      </header>