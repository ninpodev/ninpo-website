<?php include 'header.php' ?>
    
  <body class="index animsition" data-animsition-in-class="fade-in-left-sm"
  data-animsition-in-duration="600">
      
      <?php include 'modal.php' ?>
      
      <?php include 'header-menu.php' ?>
      
      
    <section id="welcome" class="container-fluid caja">
        <div class="col-lg-12">
        <h1>Design that helps you<div class="clearfix"></div><span id="rotating"></span></h1>
        </div>
        <div id="cajalin" class="caja-linea">
            <div class="linea">&nbsp;</div>
            <img src="img/view-work.svg" alt="">
        </div>
    </section>
      
      
      
      <section id="project2" class="container-fluid seccion-home">
          <div class="col-lg-12">
              <h1>InnovationACT</h1>
          </div>
          <div class="clearfix"></div>
          <div class="col-lg-12 hidden-lg hidden-md sub-hidden">
          <h2>Establishing an educational program
for Canberra’s largest entrepreneurship program.</h2>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 info">
              <p>Client</p>
              <div class="clearfix"></div>
              <p>ANU Technology Transfer Office</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 info">
              <p>Year</p>
              <div class="clearfix"></div>
              <p>2014-Present</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 info">
              <p>Type</p>
              <div class="clearfix"></div>
              <p>Program Design, Workshop Facilitation, Branding, Digital Platform Development.</p>
          </div>
          <div class="col-lg-11 col-md-11 col-sm-10 col-xs-12 bbw">
              <div class="box fleft fwidth">
              <h2 class="hidden-sm hidden-xs">Establishing an educational program for Canberra’s largest entrepreneurship program..</h2>
                  
                <a href="innovationact" class="animsition-link hidden-enter-project hidden-lg hidden-md" data-animsition-out-class="fade-out-left-sm" data-animsition-out-duration="300">
                    <img src="img/enter-icon.svg" alt=""> Enter project
                  </a>  
            </div>
          </div>
          <a href="innovationact" class="animsition-link enter-project" data-animsition-out-class="fade-out-left-sm" data-animsition-out-duration="300"><img src="img/enter-icon.svg" alt="">&nbsp; Enter project</a>
          
      </section>
      
      <section id="project3" class="container-fluid seccion-home">
          <div class="col-lg-12">
              <h1>InnovationACT</h1>
          <!--<a href="innovationact" class="animsition-link" data-animsition-out-class="fade-out-left-sm" data-animsition-out-duration="300">Project 1 &rarr;</a>-->
              
          </div>
          <div class="clearfix"></div>
          <div class="col-lg-12 hidden-lg hidden-md sub-hidden">
          <h2>Establishing an educational program
for Canberra’s largest entrepreneurship program.</h2>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 info">
              <p>Client</p>
              <div class="clearfix"></div>
              <p>ANU Technology Transfer Office</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 info">
              <p>Year</p>
              <div class="clearfix"></div>
              <p>2014-Present</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 info">
              <p>Type</p>
              <div class="clearfix"></div>
              <p>Program Design, Workshop Facilitation, Branding, Digital Platform Development.</p>
          </div>
          <!---->
          <div class="col-lg-11 col-md-11 col-sm-10 col-xs-12 bbw">
              <div class="box fleft fwidth">
              <h2 class="hidden-sm hidden-xs">Establishing an educational program for Canberra’s largest entrepreneurship program..</h2>
                  
                <a href="#" class="hidden-enter-project hidden-lg hidden-md">
                    <img src="img/enter-icon.svg" alt=""> Enter project
                  </a>  
            </div>
          </div>
          <a href="innovationact" class="animsition-link enter-project" data-animsition-out-class="fade-out-left-sm" data-animsition-out-duration="300"><img src="img/enter-icon.svg" alt="">&nbsp; Enter project</a>
          
      </section>
      

<? include 'footer.php' ?>