$(document).ready(function() {
    $(".animsition").animsition({
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: false,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });
    
    $('body').on('animsition.inEnd', function(){
        
        setTimeout(function(){
            $('#logo-header').removeClass('no-opacity');
      },500);
        
        setTimeout(function(){
            $('#button-modal').removeClass('no-opacity');
      },700);
        
        setTimeout(function(){
            $('#footer-slide').removeClass('no-opacity');
      },900);
        
    })
    
function addSlidesNavigation(section, numSlides){
        section.append('<div class="fp-slidesNav"><ul></ul></div>');
        var nav = section.find('.fp-slidesNav');

        //top or bottom
        nav.addClass(options.slidesNavPosition);

        for(var i=0; i< numSlides; i++){
            //nav.find('ul').append('<li><a href="#" title="'+ tooltip[i] +'"><span></span></a></li>');

            // Only add tooltip if needed (defined by user)
            var tooltip = options.horizontalNavigationToolTip[i];

            var li = '<li><a class="tooltips" href="#"><span></span><div><img src="images/tooltip/'+ tooltip +'.png" /></span></div></a>';
            li += '</li>';

            nav.find('ul').append(li);
        }

        //centering it
        nav.css('margin-left', '-' + (nav.width()/2) + 'px');

        nav.find('li').first().find('a').addClass('active');
    }    
    
    $('#fullpage').fullpage({
        scrollOverflow: true,
        loopHorizontal: false,
        continuousHorizontal: false,
        anchors:['project','project2'],
        css3: false,
        easing: 'easeInOutQuint',
        dragAndMove: true,
        verticalCentered: false,
        
		showActiveTooltip: true,
		slidesNavigation: false,
    });
});

//$('#flip').modal('show');

function neverStop() {
  typer('#rotating')
    .pause(500)
    .line('Generate new business')
    .pause(1000)
.run(function() {
    $('#cajalin').addClass('fullopacity');
    })
    .back('empty')
    .continue('Understand your stakeholders')
    .pause(4000)
    .back('empty')
    .continue('Clarify your vision')
    .pause(4000)
    .back('empty')
    .continue('Build a culture')
    .pause(4000)
    .back('empty')
    .continue('Establish an identity')
    .pause(4000)
    .back('empty')
    .continue('Develop your creativity')
    .pause(4000)
    .back('empty')
    .run(function(el) {
      el.innerHTML = '';
      document.body.dispatchEvent(new Event('killTyper'));
      setTimeout(() => neverStop(), 10);
    });
}    
  neverStop();  
  