<!-- Modal -->
<div id="flip" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
            <header id="header" class="container-fluid in-modal">
                <div class="col-lg-3 text-left">
                    <a href="index" class="animsition-link fleft" data-animsition-out-class="fade-out-right-sm" data-animsition-out-duration="300"> <img src="img/logo-ninpo-black.svg" alt=""></a>
                </div>
                <div class="col-lg-1 col-lg-offset-8 text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><img src="img/close-modal.svg" alt=""></button>
                </div>
            </header>
          
      </div>
        <!--modal header-->
        <div class="modal-body">
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 about">
                        <h2>About</h2>

                        <p>We are an interdisciplinary design collective with experience in branding, graphics, web, products and education.</p>
                    <br>
                        <p>We will work hard to help you achieve your organisational vision.</p>
                    </div>
                    <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-12 contact">
                        <h2>Contact</h2>

                        <p>Level 5, 1 Moore Street,<br>
                        Civic, Canberra, Australia.<br>
                        2602</p>

                        <p>Tel. <a href="tel:+614 31 250 611">+614 31 250 611<br>
                            <a href="mailto:devilhanzo@ninpodesign.com">devilhanzo@ninpodesign.com</a></p>
                      </div>
                </div>
                <div class="caja-linea">
                    
                    <div class="linea">&nbsp;</div>
                    <img src="img/the-collective.svg" alt="">
                </div>    
            </div>
            <div class="clearfix"></div>
            <div class="container-fluid">    
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item-persona">
                    <img src="img/logo-v-ward.png" alt="">
                    <p>Industrial Designer with a Master of Management, lectures entrepreneurship, loves Japan and writes short form science fiction for fun.</p>
                    <a href="#">link</a>
                    
                    <ul>
                        <li><a href="#">URL</a></li>
                        <li><a href="#">URL</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item-persona">
                    <img src="img/logo-v-ward.png" alt="">
                    <p>Industrial Designer with a Master of Management, lectures entrepreneurship, loves Japan and writes short form science fiction for fun.
                        <div class="clearfix"></div>
                        <a href="#">link</a>
                    </p>
                    
                    
                    <ul>
                        <li><a href="#">URL</a></li>
                        <li><a href="#">URL</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item-persona">
                    <img src="img/logo-v-ward.png" alt="">
                    <p>Industrial Designer with a Master of Management, lectures entrepreneurship, loves Japan and writes short form science fiction for fun.</p>
                    <a href="#">link</a>
                    
                    <ul>
                        <li><a href="#">URL</a></li>
                        <li><a href="#">URL</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item-persona">
                    <img src="img/logo-v-ward.png" alt="">
                    <p>Industrial Designer with a Master of Management, lectures entrepreneurship, loves Japan and writes short form science fiction for fun.</p>
                    <a href="#">link</a>
                    
                    <ul>
                        <li><a href="#">URL</a></li>
                        <li><a href="#">URL</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 item-persona">
                    <img src="img/logo-v-ward.png" alt="">
                    <p>Industrial Designer with a Master of Management, lectures entrepreneurship, loves Japan and writes short form science fiction for fun.</p>
                    <a href="#">link</a>
                    
                    <ul>
                        <li><a href="#">URL</a></li>
                        <li><a href="#">URL</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
